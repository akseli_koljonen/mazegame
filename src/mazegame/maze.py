class Maze(object):

    def __init__(self,width,height):
        self.height = height
        self.width = width
        self.hor_walls = []
        self.ver_walls = []

    def create_walls(self):
        # create every horizontal wall and add it to the list.

        for row in range(self.height):
            columns_temp = []
            for cell in range(self.width+1):
                columns_temp.append('w')
            self.hor_walls.append(columns_temp)

        # create every vertical wall and add it to the list.
        for row in range(self.height+1):
            columns_temp = []
            for cell in range(self.width):
                columns_temp.append('w')
            self.ver_walls.append(columns_temp)
