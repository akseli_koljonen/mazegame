from maze import Maze
from corrupted_file import *
class ChunkIO(object):

    def load_game(self, input_file):

        chunk = open(input_file, "r")
        header = self.read_fully(6, chunk)
        header = "".join(header)

        if str(header).startswith("MAZE01"):
            width = self.read_fully(3,chunk)
            width = int("".join(width))

            height = self.read_fully(3,chunk)
            height = int("".join(height))
            x= self.read_fully(3,chunk)
            x_coor = int("".join(x))
            y= self.read_fully(3,chunk)
            y_coor = int("".join(y))
            is_under = self.read_fully(1,chunk)
            is_under = int(is_under[0])
            maze = Maze(width,height)

            #load ver walls
            for x in range(height+1):
                vrow = self.read_fully(width,chunk)
                maze.ver_walls.append(vrow)
            #load hor walls
            for y in range(height):
                hrow = self.read_fully(width+1,chunk)
                maze.hor_walls.append(hrow)
        else:
            raise CorruptedMazeFileError("Unknown file type")
        chunk.close()
        return maze,width,height,x_coor,y_coor,is_under

    def save_game(self,input_file, maze,x_coor,y_coor,is_under):

        chunk = open(input_file, "w")
        chunk.write("MAZE01")
        chunk.write(format(maze.width, "03d"))
        chunk.write(format(maze.height, "03d"))
        chunk.write(format(x_coor,"03d"))
        chunk.write(format(y_coor,"03d"))
        #check if under
        if is_under:
            temp = 1
        else:
            temp = 0
        chunk.write(format(temp,"01d"))
        #save ver walls
        for x in range(maze.height+1):
            wrow = maze.ver_walls[x]
            wrow = "".join(wrow)
            chunk.write(wrow)
        #c
        for y in range(maze.height):
            hrow = maze.hor_walls[y]
            hrow = "".join(hrow)
            chunk.write(hrow)
        chunk.close()


    def read_fully(self,count,chunk):
        read = chunk.read(count)
        if len(read) != count:
            raise CorruptedMazeFileError("Unexpected end of file.")
        return list(read)
