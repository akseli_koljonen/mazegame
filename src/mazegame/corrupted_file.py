class CorruptedMazeFileError(Exception):

    def __init__(self, message):
        super(CorruptedMazeFileError, self).__init__(message)
