from mazesolver import MazeSolver
from mazecreator import MazeCreator
from maze import Maze
from player import Player
from chunkIO import ChunkIO
import sys
import os
from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox,QFrame,QPushButton,QHBoxLayout, QVBoxLayout, QLabel, QMainWindow,QAction,qApp,QFileDialog
from PyQt5.QtGui import QPainter, QColor, QFont, QKeyEvent,QPen,QIcon,QImage,QPixmap
from PyQt5.QtCore import Qt, QTimer, QDir

class MainWindow(QMainWindow):
    def __init__(self,w,h):
        super().__init__()
        self.width = int(w)
        self.height = int(h)
        self.init_gamemenu()
        self.chunkio = ChunkIO()

    def init_gamemenu(self):
        #create mainwindow
        newgameButton = QPushButton("New Game", self)
        newgameButton.move(30, 50)
        newgameButton.clicked.connect(self.new_game)
        exitButton = QPushButton("Exit",self)
        exitButton.move(150, 50)
        exitButton.clicked.connect(qApp.quit)
        loadgameButton = QPushButton("Load Game", self)
        loadgameButton.move(150, 80)
        loadgameButton.clicked.connect(self.load_game)
        self.statusBar()
        self.setGeometry(500, 300, 290, 150)
        self.setWindowTitle('MainMenu')
        self.show()

    def load_game(self):

        f = QFileDialog.getOpenFileName(self, "Load Maze",
                                                           ".", "*.lab")
        file_name = QDir.toNativeSeparators(f[0])
        if file_name == '':
            ex = MainWindow(self.width,self.height)
        else:
            self.load,self.width,self.height,self.x,self.y,self.is_under = self.chunkio.load_game(file_name)
            self.player = Player()
            self.player.place_player(self.x,self.y,self.is_under,self.load)
            self.luovutus = False
            self.solver = MazeSolver()
            #find solution
            self.solution = self.solver.solve_maze(self.load, 0, 0,
                                              False, self.load.height-1,
                                              self.load.width-1)
            #find solution to the key
            self.solution_checkpoint = self.solver.solve_maze(self.load, 0, 0,
                                              False, self.load.height-1,
                                              0)
            self.w = Draw(self.load,self.player,self.time,self.width,self.height,self.solver)
            self.w.show()

    def save_game(self,maze,x,y,is_under):
        self.maze = maze
        self.x = x
        self.y = y
        self.is_under = is_under
        f = QFileDialog.getSaveFileName(self, "Save Maze",
                                                  ".", "*.lab")

        file_name = QDir.toNativeSeparators(f[0])
        if file_name != "":
            self.chunkio.save_game(file_name,self.maze,self.x,self.y,self.is_under)

    def new_game(self):
        self.time = 0
        self.luovutus = False
        creator = MazeCreator()
        self.maze = creator.create_maze(self.width,self.height)
        self.player = Player()
        self.player.place_player(0,0,False,self.maze)
        self.solver = MazeSolver()
        #find solution
        self.solution = self.solver.solve_maze(self.maze, 0, 0,
                                          False, self.maze.height-1,
                                          self.maze.width-1)
        #find solution to the key
        self.solution_checkpoint = self.solver.solve_maze(self.maze, 0, 0,
                                          False, self.maze.height-1,
                                          0)
        #create a perfect maze
        while not (self.solution and self.solution_checkpoint):
            print('Lataa...')
            self.maze = creator.create_maze(self.width,self.height)
            self.solution = self.solver.solve_maze(self.maze, 0, 0,
                                              False, self.maze.height-1,
                                              self.maze.width-1)
            self.solution_checkpoint = self.solver.solve_maze(self.maze, 0, 0,
                                              False, self.maze.height-1,
                                              0)
        self.w = Draw(self.maze,self.player,self.time,self.width,self.height,self.solver)
        self.w.show()

class Draw(QWidget):

    def __init__(self,maze,player,time,width,height,solver):
        self.width = width
        self.height = height
        self.CheckFirst = False
        self.line = 2
        self.wall = 6
        self.square = 10
        self.line = 3
        self.goal_x = self.width-1
        self.goal_y = self.height-1
        self.luovutus = False
        super().__init__()
        self.setFocusPolicy(Qt.StrongFocus)
        self.maze = maze
        self.player = player
        self.time = time
        self.solver = solver
        self.chunkio = ChunkIO()
        self.initUI()

    def initUI(self):
        self.setGeometry(400, 150, self.width*(self.square+self.wall)+self.wall+80,
        self.height*(self.square+self.wall)+self.wall+40)
        self.qtime = QTimer()
        self.qtime.timeout.connect(self.update_time)
        self.qtime.start(100)
        self.setWindowTitle('maze')
        path = os.path.join(os.path.dirname(sys.modules[__name__].__file__), 'img/key.png')
        key_image = QImage(path) #picture of the key
        pix=QPixmap.fromImage(key_image)
        self.lbl = QLabel(self)
        self.lbl.setPixmap(pix)
        self.lbl.move(5, self.height*15.5)


    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        self.drawRect(qp)
        self.drawText(event, qp)
        if self.luovutus:
            self.draw_solution(qp)
            if not self.CheckFirst:
                self.draw_solution_checkpoint(qp)
        self.drawPlayer(qp)
        qp.end()


    def update_time(self):
        self.time += 0.1
        self.update()

    def get_solution(self):
        self.luovutus = True
        x, y, under = self.player.get_coordinates()
        self.solution = self.solver.solve_maze(self.maze, y, x,
                                          False, self.maze.height-1,
                                          self.maze.width-1)
        self.solution_checkpoint = self.solver.solve_maze(self.maze, y, x,
                                          False, self.maze.height-1,
                                          0)
        self.update()

    def drawRect(self, qp):
        #thickness of the wall
        wall = self.wall
        square = self.square
        line = self.line
        qp.setBrush(Qt.black)

        #draw the grid
        for y in range(self.height+1):
            qp.drawRect(0, y*(square+wall), (self.width)*(square+wall)+wall, wall)
        for x in range(self.width + 1):
            qp.drawRect(x*(square+wall), 0, wall, (self.height)*(square+wall)+wall)

        #delete walls
        for y in range(self.height):
            for x in range(self.width+1):
                    if self.maze.hor_walls[y][x]=='o':
                        qp.eraseRect(x*(square+wall), wall+y*(square+wall)+1, wall+1, square-1)
                    if self.maze.hor_walls[y][x] == 's':
                        qp.eraseRect(x*(square+wall), wall+y*(square+wall)+1, line+1, square-1)
                    if self.maze.hor_walls[y][x] == 'u':
                        qp.eraseRect(x*(square+wall)+3, wall+y*(square+wall)+1, line+2, square-1)
                        #qp.eraseRect(w+x*(s+w)+1, y*(s+w)+5, s-1, l+5)

        for y in range(self.height+1):
            for x in range(self.width):
                    if self.maze.ver_walls[y][x]=='o':
                        qp.eraseRect(wall+x*(square+wall)+1, y*(square+wall), square-1, wall+1)
                    if self.maze.ver_walls[y][x] == 's':
                        qp.eraseRect(wall+x*(square+wall)+1, y*(square+wall), square-1, line+1)

                    if self.maze.ver_walls[y][x] == 'u':
                        qp.eraseRect(wall+x*(square+wall)+1, y*(square+wall)+3, square-1, line+2)
                        #qp.eraseRect(w+x*(s+w)+1, y*(s+w)+5, s-1, l+5)

        qp.setBrush(Qt.red)
        #draw the goal
        if self.CheckFirst:
            qp.setBrush(Qt.green)
        qp.drawRect(wall+(self.width-1)*(square+wall), wall+(self.height-1)*(square+wall), square-1, square-1)

    def draw_solution(self,qp):
        #thickness of wall
        wall = self.wall
        square = self.square
        x = None
        y = None
        for lista in self.solution:
            self.update()
            pen = QPen(Qt.red, 4, Qt.SolidLine)
            qp.setPen(pen)
            if x != None:
                qp.drawLine(wall+lista[0]*(square+wall)+5,wall+lista[1]*(square+wall)+5,wall+y*(square+wall)+5,wall+x*(square+wall)+5)
            x = lista[1]
            y = lista[0]

    def draw_solution_checkpoint(self,qp):
        #thickness of wall
        wall = self.wall
        square = self.square
        x = None
        y = None
        for lista in self.solution_checkpoint:
            self.update()
            pen = QPen(Qt.green, 4, Qt.SolidLine)
            qp.setPen(pen)
            if x != None:
                qp.drawLine(wall+lista[0]*(square+wall)+5,wall+lista[1]*(square+wall)+5,wall+y*(square+wall)+5,wall+x*(square+wall)+5)
            x = lista[1]
            y = lista[0]

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Left:
            if self.player.try_left(self.maze.hor_walls):
                self.update()
                self.test_victory()
                self.test_checkpoint()
            return
        elif key == Qt.Key_Right:
            if self.player.try_right(self.maze.hor_walls):
                self.update()
                self.test_victory()
                self.test_checkpoint()
            return
        elif key == Qt.Key_Up:
            if self.player.try_up(self.maze.ver_walls):
                self.update()
                self.test_victory()
                self.test_checkpoint()
            return
        elif key == Qt.Key_Down:
            if self.player.try_down(self.maze.ver_walls):
                self.update()
                self.test_victory()
                self.test_checkpoint()
            return
        elif key == Qt.Key_L:
            QMessageBox.information(self, "Nössö", "Luovutit! ")
            self.get_solution()
            self.lost_event()
        elif key == Qt.Key_S:
            self.qtime.stop()
            main = MainWindow(self.width, self.height)
            x, y, is_under = self.player.get_coordinates()
            self.save = main.save_game(self.maze,x,y,is_under)


    def test_checkpoint(self):
        #test if player is on the checkpoint
        x, y, is_under = self.player.get_coordinates()
        if x == 0 and y == self.goal_y:
            self.CheckFirst = True
            self.removeCheckPoint()

    def removeCheckPoint(self):
        #hide the key
        self.lbl.hide()

    def drawText(self, event, qp):
        steps = self.player.get_steps()
        steptext = 'Stepit: {}'.format(steps)
        qp.setFont(QFont('Decorative', 20))
        qp.drawText(event.rect(), Qt.AlignTop | Qt.AlignRight, steptext)
        timetext = 'Aika: {:.1f}s'.format(self.time)
        qp.drawText(event.rect(), Qt.AlignBottom, timetext)
        surrtext = 'Luovuta painamalla L'
        qp.drawText(event.rect(), Qt.AlignRight|Qt.AlignBottom, surrtext)

    def drawPlayer(self, qp):
        wall = self.wall
        square = self.square
        qp.setBrush(Qt.red)
        x, y, is_under = self.player.get_coordinates()
        if is_under:
            qp.setBrush(0)
        qp.drawEllipse(wall+x*(square+wall), wall+y*(square+wall), square-1, square-1)


    def test_victory(self):
        x, y, is_under = self.player.get_coordinates()
        if x == self.goal_x and y == self.goal_y:
            if self.CheckFirst:
                self.victory_event()
            else:
                QMessageBox.information(self, "Urpo", "Etsi ensiksi avain!")

    def victory_event(self):
        self.qtime.stop()
        steps = self.player.get_steps()
        QMessageBox.information(self, "HYVÄ", "Ratkaisit labyrintin ajassa: {:.1f} ja siihen meni {} askelta!!".format(self.time, steps))

    def lost_event(self):
        self.qtime.stop()
        steps = self.player.get_steps()
        QMessageBox.information(self, "HUONO", "Ei se nyt iha putkee menny. Luovutit ajassa: {:.1f} ja käytit {} askelta!!".format(self.time, steps))

if __name__ == '__main__':
    w = input("Labyrintin leveys: ")
    h = input("Labyrintin korkeus: ")
    while int(w) > 40 or int(h) > 40:
        print("Liian iso labyrintti. Korkeus eikä leveys saa olla yli 40")
        w = input("Labyrintin leveys: ")
        h = input("Labyrintin korkeus: ")
    app = QApplication(sys.argv)
    path = os.path.join(os.path.dirname(sys.modules[__name__].__file__), 'img/maze.png')
    app.setWindowIcon(QIcon(path))
    ex = MainWindow(w,h)
    sys.exit(app.exec_())
