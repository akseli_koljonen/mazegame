from mazecreator import MazeCreator
from maze import Maze
from player import Player
class MazeSolver(object):


    def __init__(self):
        "asdf"

    def solve_maze(self, maze, start_y, start_x, under, end_y, end_x):
        self.maze = maze
        self.y = start_y
        self.x = start_x
        self.under = under
        self.player = Player()
        self.solution = []
        timeout_counter = 0
        timeout_max = 4000
        self.direction = 'l'
        self.solution.append([self.x, self.y, self.under])

        while not (self.x == end_x and self.y == end_y):
            #if timeout is greater than timeout_max, a new maze will be made.
            timeout_counter += 1
            if timeout_counter < timeout_max:
                self.step()
                self.record_path()
            else:
                return False

        return self.solution

    #Left Hand algorithm
    def step(self):
        if self.direction == "l":
            if self.try_left():
                self.direction = "u"
            else:
                self.direction = "d"
            return

        if self.direction == "d":
            if self.try_down():
                self.direction = "l"
            else:
                self.direction = "r"
            return

        if self.direction == "r":
            if self.try_right():
                self.direction = "d"
            else:
                self.direction = "u"
            return

        if self.direction == "u":
            if self.try_up():
                self.direction = "r"
            else:
                self.direction = "l"
            return

    def record_path(self):
        step = [self.x, self.y, self.under]
        if step in self.solution:
            while self.solution.pop() != step:
                "Keep removing!"
        self.solution.append(step)

    def try_left(self):
        if self.maze.hor_walls[self.y][self.x] == 'o':
            if not self.under:
                self.x -= 1
                return True
        elif self.maze.hor_walls[self.y][self.x] == 's':
            if self.under:
                self.x -= 1
                self.under = False
                return True
        elif self.maze.hor_walls[self.y][self.x] == 'u':
            if not self.under:
                self.x -= 1
                self.under = True
                return True
        else:
            return False

    def try_right(self):
        if self.maze.hor_walls[self.y][self.x+1] == 'o':
            if not self.under:

                self.x += 1
                return True
        elif self.maze.hor_walls[self.y][self.x+1] == 's':
            if not self.under:
                self.x += 1
                self.under = True
                return True
        elif self.maze.hor_walls[self.y][self.x+1] == 'u':
            if self.under:
                self.x += 1
                self.under = False
                return True
        else:
            return False

    def try_up(self):
        if self.maze.ver_walls[self.y][self.x] == 'o':
            if not self.under:
                self.y -= 1
                return True
        elif self.maze.ver_walls[self.y][self.x] == 's':
            if self.under:
                self.y -= 1
                self.under = False
                return True
        elif self.maze.ver_walls[self.y][self.x] == 'u':
            if not self.under:
                self.y -= 1
                self.under = True
                return True
        else:
            return False

    def try_down(self):
        if self.maze.ver_walls[self.y+1][self.x] == 'o':
            if not self.under:
                self.y += 1

                return True
        elif self.maze.ver_walls[self.y+1][self.x] == 's':
            if not self.under:
                self.y += 1
                self.under = True
                return True
        elif self.maze.ver_walls[self.y+1][self.x] == 'u':
            if self.under:
                self.y += 1
                self.under = False
                return True
        else:
            return False
