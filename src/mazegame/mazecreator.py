import random
from maze import Maze


class MazeCreator(object):

    def create_maze(self,width,height):
        self.maze = Maze(width,height)
        self.maze.create_walls()
        self.walls = self.create_walltable()
        self.cell_sets = self.create_sets()
        self.walls_copy = self.walls[:]
        random.shuffle(self.walls_copy) #randomize walltable
        self.generate_maze = self.kruskal_algorithm()
        return self.generate_maze

    def kruskal_algorithm(self):
        # in this algorithm, we want to visit every wall in order of weight
        # we want a random maze, so we will shuffle the walltable
        # and pretend that they are sorted by weight
        for wall in self.walls_copy:
            set_a = None
            set_b = None
            set_c = None
            set_d = None
            for s in self.cell_sets:
                if (wall[0], wall[1]) in s:
                    set_a = s
                if (wall[2], wall[3]) in s:
                    set_b = s
                if ((wall[0])+2, wall[1]) in s:
                    set_c = s
                if (wall[0], (wall[1])+2) in s:
                    set_d = s
                    
            #check if same set
            if set_a is not set_b:
                self.cell_sets.remove(set_a)
                self.cell_sets.remove(set_b)
                self.cell_sets.append(set_a.union(set_b))
                self.walls.remove(wall)
                #check if vertical wall
                if wall[1]==wall[3] and self.maze.ver_walls[wall[2]][wall[3]]=='w':
                    self.maze.ver_walls[wall[2]][wall[3]]='o'
                #check if horizontal wall
                elif wall[0]==wall[2] and self.maze.hor_walls[wall[2]][wall[3]]=='w':
                    self.maze.hor_walls[wall[2]][wall[3]]='o'
                #create horizontal tunnel
            elif set_a is not set_d and wall[0]==wall[2] and set_d != None:
                self.cell_sets.remove(set_a)
                self.cell_sets.remove(set_d)
                self.cell_sets.append(set_a.union(set_d))
                self.walls.remove(wall)

                if self.maze.hor_walls[wall[2]][wall[3]]=='w' and self.maze.hor_walls[wall[2]][wall[3]+1]=='w':
                    self.maze.hor_walls[wall[2]][wall[3]]='s'
                    self.maze.hor_walls[wall[2]][wall[3]+1]='u'
                #create vertical tunnel
            elif set_a is not set_c and wall[1]==wall[3] and set_c != None:
                self.cell_sets.remove(set_a)
                self.cell_sets.remove(set_c)
                self.cell_sets.append(set_a.union(set_c))
                self.walls.remove(wall)

                if self.maze.ver_walls[wall[2]][wall[3]]=='w' and self.maze.ver_walls[wall[2]+1][wall[3]]=='w':
                    self.maze.ver_walls[wall[2]][wall[3]]='s'
                    self.maze.ver_walls[wall[2]+1][wall[3]]='u'
        return self.maze

    def create_walltable(self):
        #create verical walls
        walls = [(y, x, y, x+1)
            for x in range(self.maze.width-1)
            for y in range(self.maze.height)]
        #create horizontal walls
        walls.extend([(y,x,y+1,x)
            for x in range(self.maze.width)
            for y in range(self.maze.height-1)])

        return walls


    def create_sets(self):
        #create sets for each cell
        cell_sets = [set([(y,x)])
            for x in range(self.maze.width)
            for y in range(self.maze.height)]
        return cell_sets
