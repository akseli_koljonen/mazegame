class Player(object):

    def place_player(self, x, y,is_under,maze):
        self.maze = maze
        self.x = x
        self.y = y
        self.is_under = is_under
        #check if under or not from the loaded file
        if self.is_under == 1:
            self.is_under = True
        else:
            self.is_under = False

        self.steps = 0

    def try_left(self, hor_wall):
        if hor_wall[self.y][self.x] == 'o':
            if not self.is_under:
                self.x -= 1
                self.steps += 1
                return True
        elif hor_wall[self.y][self.x] == 's':
            if self.is_under:
                self.x -= 1
                self.is_under = False
                self.steps += 1
                return True
        elif hor_wall[self.y][self.x] == 'u':
            if not self.is_under:
                self.x -= 1
                self.is_under = True
                self.steps += 1
                return True
        else:
            return False

    def try_right(self, hor_wall):
        if hor_wall[self.y][self.x+1] == 'o':
            if not self.is_under:
                self.x += 1
                self.steps += 1
                return True
        elif hor_wall[self.y][self.x+1] == 's':
            if not self.is_under:
                self.x += 1
                self.is_under = True
                self.steps += 1
                return True
        elif hor_wall[self.y][self.x+1] == 'u':
            if self.is_under:
                self.x += 1
                self.is_under = False
                self.steps += 1
                return True
        else:
            return False

    def try_up(self, ver_wall):
        if ver_wall[self.y][self.x] == 'o':
            if not self.is_under:
                self.y -= 1
                self.steps += 1
                return True
        elif ver_wall[self.y][self.x] == 's':
            if self.is_under:
                self.y -= 1
                self.is_under = False
                self.steps += 1
                return True
        elif ver_wall[self.y][self.x] == 'u':
            if not self.is_under:
                self.y -= 1
                self.is_under = True
                self.steps += 1
                return True
        else:
            return False

    def try_down(self, ver_wall):
        if ver_wall[self.y+1][self.x] == 'o':
            if not self.is_under:
                self.y += 1
                self.steps += 1
                return True
        elif ver_wall[self.y+1][self.x] == 's':
            if not self.is_under:
                self.y += 1
                self.is_under = True
                self.steps += 1
                return True
        elif ver_wall[self.y+1][self.x] == 'u':
            if self.is_under:
                self.y += 1
                self.is_under = False
                self.steps += 1
                return True
        else:
            return False

    def get_coordinates(self):
        if self.x > self.maze.width or self.x < 0 or self.y > self.maze.height or self.y < 0:
            raise CorruptedMazeFileError("Unexpected player's position")

        return self.x, self.y, self.is_under

    def get_steps(self):
        return self.steps
